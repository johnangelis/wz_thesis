EX              := wz_event_selector addHistos
EXE             := $(addsuffix .exe,$(EX))
ROOTCXXFLAGS    := $(shell root-config --cflags --glibs)


all: wz_event_selector addHistos

readingtest: wz_event_selector.cpp WZhelper.h
	$(CXX) -O3 -g $< -o $@.exe $(ROOTCXXFLAGS)

addHistos: addHistos.cpp
	$(CXX) -O3 -g $< -o $@.exe $(ROOTCXXFLAGS)
