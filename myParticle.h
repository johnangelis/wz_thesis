#pragma once

#include <ROOT/TLorentzVector.h>
#include <ROOT/TMath.h>



class myParticle
{
  public:
    // myParticle() {}
    myParticle()
        : pdg_id(0), px(0), py(0), pz(0), e(0)
    {
        pvec4.SetPxPyPzE(0, 0, 0, 0);
    } // default constructor
    
    myParticle(int _id, double _px, double _py, double _pz, double _e)
        : pdg_id(_id), px(_px), py(_py), pz(_pz), e(_e)
    {
        pvec4.SetPxPyPzE(_px, _py, _pz, _e);
    }

    myParticle(const int &_id, const TLorentzVector &p)
        : pdg_id(_id), pvec4(p)
    {
    }

    myParticle(const myParticle &x)
        : pdg_id(x.getId()), pvec4(x.getVec())
    {
    }

    int getId() const
    {
        return pdg_id;
    }

    TLorentzVector getVec() const
    {
        return pvec4;
    }

    double Px() const
    {
        return pvec4.Px();
    }
    double Py() const
    {
        return pvec4.Py();
    }
    double Pz() const
    {
        return pvec4.Pz();
    }
    double E() const
    {
        return pvec4.E();
    }

    double Eta() const
    {
        return pvec4.Eta();
    }

    double Pt() const
    {
        return pvec4.Pt();
    }

    double Rapidity() const
    {
        return pvec4.Rapidity();
    }

    double DeltaPhi(const myParticle &p) const
    {
        return pvec4.DeltaPhi(p.pvec4);
    }
    double DeltaPhi(const TLorentzVector &v) const
    {
        return pvec4.DeltaPhi(v);
    }

    double DeltaR(const myParticle &p) const
    {
        return pvec4.DeltaR(p.pvec4);
    }

    double M() const
    {
        return pvec4.M();
    }

    double Mt() const
    {
        return pvec4.Mt();
    }

    void clear()
    {
        pdg_id = 0;
        pvec4 = {0, 0, 0, 0};
    }

    myParticle operator+(const myParticle &p) const
    {
        return myParticle(0, pvec4 + p.pvec4);
    }

    myParticle operator+(const TLorentzVector &v) const
    {
        return myParticle(0, pvec4 + v);
    }

    bool operator==(const myParticle &p) const
    {
        return (pdg_id == p.getId() && pvec4 == p.pvec4);
    }

    bool operator!=(const myParticle &p) const
    {
        return (pdg_id != p.getId() || pvec4 != p.pvec4);
    }
    ~myParticle()
    {
        pdg_id = 0;
        pvec4.Clear();
    }

  private:
    int pdg_id;
    double px, py, pz, e;
    TLorentzVector pvec4;
};
