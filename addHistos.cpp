#include <iostream>
#include <stdexcept>
#include <fstream>
#include <cmath>
#include <string>
#include <iomanip>
#include <time.h>
#include <vector>
#include <algorithm>
#include <ROOT/TTree.h>
#include <ROOT/TCanvas.h>
#include <ROOT/TFile.h>
#include <ROOT/TH1.h>
#include <ROOT/TLorentzVector.h>
#include <ROOT/TMath.h>
#include <ROOT/TClassRef.h>
#include <ROOT/TBranchElement.h>
#include <ROOT/TStyle.h>
#include <ROOT/THStack.h>
#include <ROOT/TLegend.h>

using namespace std;

constexpr double lumi_prime = 36.1; // L = 36.1 fb

void addHistos()
{

    vector<string> names;
    names.push_back("m_{Z}, Z->ee");
    names.push_back("m_{Z}, Z->#mu#mu");
    names.push_back("m_{Z}");
    names.push_back("p_{T}^{Z}");
    names.push_back("m_{T}^{W}");
    names.push_back("p_{T}^{W}");
    names.push_back("m_{T}^{WZ}");
    names.push_back("m_{T}^{WZ}, Z->ee");
    names.push_back("m_{T}^{WZ}, Z->#mu#mu");
    names.push_back("m_{T}^{WZ}, W^{+}");
    names.push_back("m_{T}^{WZ}, W^{-}");
    names.push_back("m_{T}^{WZ}, W^{+},Z->ee");
    names.push_back("m_{T}^{WZ}, W^{-},Z->ee");
    names.push_back("m_{T}^{WZ}, W^{+},Z->#mu#mu");
    names.push_back("m_{T}^{WZ}, W^{-},Z->#mu#mu");
    names.push_back("m_{T}^{WZ}, WZ->eee");
    names.push_back("m_{T}^{WZ}, WZ->#mu ee");
    names.push_back("m_{T}^{WZ}, WZ->#mu#mu#mu");
    names.push_back("m_{T}^{WZ}, WZ->e#mu#mu");
    names.push_back("m_{lll}");
    names.push_back("#Delta R");
    names.push_back("#Delta y WZ");
    names.push_back("fake #nu");
    names.push_back("real #nu");

    gStyle->SetOptStat(0);
    TFile *i_file1 = new TFile("./used_files/wz_sherpa2_output.root", "READ");
    TFile *i_file2 = new TFile("./used_files/zz_bkg_output.root", "READ");
    TFile *i_file3 = new TFile("./used_files/gg2tt_ptvarcone30_jetiso02_output.root", "READ");
    TFile *i_file4 = new TFile("./used_files/ttee_output.root", "READ");
    TFile *i_file5 = new TFile("./used_files/ttmm_output.root", "READ");
    TFile *i_file6 = new TFile("./used_files/ttW_output.root", "READ");
    TFile *i_file7 = new TFile("./used_files/z1j_sherpa_output.root", "READ");

    vector<TFile *> files;
    vector<THStack *> stack_vector;
    vector<TH1D *> histos_signal;
    vector<TH1D *> histos_zz, histos_tt, histos_ttee, histos_ttmm, histos_ttw, histos_z1j, histos_back7;
    vector<double> sigmas;
    int num_histo = (int)i_file1->GetListOfKeys()->GetSize() - 1; // Getting the number of histograms from root file

    files.push_back(i_file1);
    files.push_back(i_file2);
    files.push_back(i_file3);
    files.push_back(i_file4);
    files.push_back(i_file5);
    files.push_back(i_file6);
    files.push_back(i_file7);

    for (auto &fl : files)
    {
        TTree *info_tree = nullptr;
        fl->GetObject("sigma", info_tree);
        double cross_section, cross_section_error;
        info_tree->SetBranchAddress("cross_section", &cross_section);
        info_tree->SetBranchAddress("cross_section_error", &cross_section_error);
        info_tree->GetEntry(0);
        sigmas.push_back(cross_section);
    }

    for (int i = 0; i < num_histo; i++)
    {
        histos_signal.push_back((TH1D *)i_file1->Get(i_file1->GetListOfKeys()->At(i)->GetName()));
        histos_zz.push_back((TH1D *)i_file2->Get(i_file2->GetListOfKeys()->At(i)->GetName()));
        histos_tt.push_back((TH1D *)i_file3->Get(i_file3->GetListOfKeys()->At(i)->GetName()));
        histos_ttee.push_back((TH1D *)i_file4->Get(i_file4->GetListOfKeys()->At(i)->GetName()));
        histos_ttmm.push_back((TH1D *)i_file5->Get(i_file5->GetListOfKeys()->At(i)->GetName()));
        histos_ttw.push_back((TH1D *)i_file6->Get(i_file6->GetListOfKeys()->At(i)->GetName()));
        histos_z1j.push_back((TH1D *)i_file7->Get(i_file7->GetListOfKeys()->At(i)->GetName()));
    }

    //  loop for normalizing for 36 fb^-1
    for (int i = 0; i < num_histo; i++)
    {
        histos_signal[i]->Scale(lumi_prime * sigmas[0] * 1.65 * 1E12 / 1e6);
        histos_zz[i]->Scale(lumi_prime /* sigmas[1] * 1E12 */ * 0.15 * 1E3 / 1e6);
        histos_tt[i]->Scale(lumi_prime * sigmas[2] * 1E12 / 1e6);
        histos_ttee[i]->Scale(lumi_prime * sigmas[3] * 1E12 / 300000);
        histos_ttmm[i]->Scale(lumi_prime * sigmas[4] * 1E12 / 300000);
        histos_ttw[i]->Scale(lumi_prime * sigmas[5] * 1E12 / 1e6);
        histos_z1j[i]->Scale(lumi_prime * sigmas[6] * 0.01 * 1E12 * 1.3 / 1e6);
    }

    ofstream ofile;
    ofile.open("total_stats.dat", ios::binary);
    for (int i = 0; i < num_histo; i++)
    {
        ofile << "wz \t" << setw(20) << histos_signal[i]->GetName() << "\t" << setw(20) << histos_signal[i]->Integral() << endl;
        ofile << "zz \t" << setw(20) << histos_zz[i]->GetName() << "\t" << setw(20) << histos_zz[i]->Integral() << endl;
        ofile << "tt \t" << setw(20) << histos_tt[i]->GetName() << "\t" << setw(20) << histos_tt[i]->Integral() << endl;
        ofile << "ttV \t" << setw(20) << histos_ttee[i]->GetName() << "\t" << setw(20) << histos_ttee[i]->Integral() + histos_ttmm[i]->Integral() + histos_ttw[i]->Integral() << endl;
        ofile << "z+1j \t" << setw(20) << histos_z1j[i]->GetName() << "\t" << setw(20) << histos_z1j[i]->Integral() << endl;
        ofile << "------------------------------------------------------------------- \n";
    }
    ofile.close();

    for (int i = 0; i < num_histo; i++)
    {
        string stack_name = histos_signal[i]->GetTitle();
        string sname = "hs" + to_string(i);
        THStack *hs = new THStack(sname.c_str(), "");
        histos_ttee[i]->Add(histos_ttmm[i]);
        histos_ttee[i]->Add(histos_ttw[i]);
        histos_tt[i]->Add(histos_z1j[i]);

        histos_zz[i]->SetFillColor(kRed);
        histos_tt[i]->SetFillColor(kGreen);
        histos_ttee[i]->SetFillColor(kBlue);

        hs->Add(histos_ttee[i]);
        hs->Add(histos_tt[i]);
        hs->Add(histos_zz[i]);

        stack_vector.push_back(hs);
    }

    for (int i = 0; i < num_histo; i++)
    {
        TLegend *l = new TLegend(0.6, 0.6, 0.9, 0.9);
        l->SetHeader(names[i].c_str(), "C");
        histos_zz[i]->SetFillColor(kRed);
        histos_tt[i]->SetFillColor(kGreen);
        histos_ttee[i]->SetFillColor(kBlue);
        histos_z1j[i]->SetFillColor(kCyan);
        histos_ttw[i]->SetFillColor(kYellow);
        TCanvas *c1 = new TCanvas("c1", "test", 2400, 1440);
        l->AddEntry(histos_signal[i], "WZ", "f");
        l->AddEntry(histos_zz[i], "ZZ", "f");
        l->AddEntry(histos_tt[i], "Misid", "f");
        l->AddEntry(histos_ttee[i], "ttV", "f");
        l->SetTextFont(42);

        stack_vector[i]->SetMaximum(histos_signal[i]->GetMaximum());
        stack_vector[i]->SetMinimum(0.0);
        stack_vector[i]->Draw(" HIST");
        histos_signal[i]->Draw("SAME HIST");

        stack_vector[i]->GetXaxis()->SetTitle(histos_signal[i]->GetXaxis()->GetTitle());
        stack_vector[i]->GetYaxis()->SetTitle(histos_signal[i]->GetYaxis()->GetTitle());
        stack_vector[i]->GetYaxis()->SetTitleOffset(0.9);
        stack_vector[i]->GetYaxis()->SetTitleSize(0.05);
        stack_vector[i]->GetXaxis()->SetTitleSize(0.045);
        stack_vector[i]->GetYaxis()->SetLabelSize(0.05);
        stack_vector[i]->GetXaxis()->SetLabelSize(0.05);

        l->Draw();
        c1->Update();
        gPad->Modified();
        gPad->Update();

        string hname = histos_signal[i]->GetName();
        string name = "./plots/histo_" + hname + ".pdf";
        c1->Print(name.c_str(), "pdf");
        delete c1;
        delete l;
    }
    
    TObjArray Hlist(0);
    for_each(stack_vector.begin(), stack_vector.end(), [&Hlist](THStack *x) { Hlist.Add(x); });

    TFile output_file("output_histos.root", "RECREATE");
    Hlist.Write();
    // output_file.Write();
    output_file.Close();

    for (int i = 0, range = histos_zz.size(); i < range; i++)
    {
        delete histos_signal[i];
        delete histos_zz[i];
        delete histos_tt[i];
        delete histos_ttee[i];
        delete histos_ttmm[i];
        delete histos_ttw[i];
        delete histos_z1j[i];
    }

    for (auto &fl : files)
    {
        fl->Close();
        delete fl;
    }
}

int main()
{
    addHistos();
    return 0;
}