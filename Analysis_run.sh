#!/bin/bash

./readingtest.exe used_files/gg2tt_ptvarcone30_jetiso02 used_files/ttW used_files/ttee used_files/ttmm used_files/z1j_sherpa used_files/zz_bkg used_files/wz_sherpa2
echo Analysis of data samples done!
echo Starting scaling and histo analysis:
./addHistos.exe
# python3.6 used_files/txt_combiner.py