#include <iostream>
#include <fstream>
#include <stdexcept>
#include <cmath>
#include <string>
#include <iomanip>
#include <chrono>
#include <time.h>
#include <vector>
#include <algorithm>
#include <ROOT/TTree.h>
#include <ROOT/TCanvas.h>
#include <ROOT/TFile.h>
#include <ROOT/TH1.h>
#include <ROOT/TH2.h>
#include <ROOT/TLorentzVector.h>
#include <ROOT/TMath.h>
#include <ROOT/TClassRef.h>
#include <ROOT/TBranchElement.h>
#include <ROOT/TStyle.h>
#include "WZhelper.h"

using namespace std;

void wz_event_selector(string str)
{
    const auto start = chrono::steady_clock::now();

    // reading TTree from root file
    string filename = str + ".root";
    TFile i_file(filename.c_str(), "READ");
    TTree *lepton_tree = nullptr, *info_tree = nullptr;
    i_file.GetObject("leptons", lepton_tree);
    i_file.GetObject("mcinfo", info_tree);

    ofstream ofile;
    ofile.open(str + "_output.txt");

    int nEntries = (int)lepton_tree->GetEntries();
    gStyle->SetOptStat(11111111);
    // // Initializing Canvases
    TCanvas *c1 = new TCanvas("c1", "Z Canvas", 2400, 1440);
    c1->Divide(3, 2);
    TCanvas *c2 = new TCanvas("c2", "WZ Canvas", 2400, 1440);
    c2->Divide(3, 2);
    TCanvas *c3 = new TCanvas("c3", " ",2400, 1440);
    c3->Divide(3, 2);
    // TCanvas *c4 = new TCanvas("c4", "WZ ", 1200, 720);
    // c4->Divide(2);
    TCanvas *c5 = new TCanvas("c5", "WZ channels", 2400, 1440);
    c5->Divide(2, 2);
    TCanvas *c6 = new TCanvas("c6", " stuff ", 2400, 1440);
    c6->Divide(2, 2);
    TCanvas *c7 = new TCanvas("c7", "Neutrino Pt", 2400, 1440);
    c7->Divide(2, 2);

    // variables for reading cross section tree
    double cross_section;
    double cross_section_error;
    // Declaring variables for reading the leptons tree
    int num_of_leptons, num_of_neutrinos;
    vector<int> *lep_ids = new vector<int>;
    vector<double> *lep_px = new vector<double>;
    vector<double> *lep_py = new vector<double>;
    vector<double> *lep_pz = new vector<double>;
    vector<double> *lep_e = new vector<double>;

    info_tree->SetBranchAddress("cross_section", &cross_section);
    info_tree->SetBranchAddress("cross_section_error", &cross_section_error);
    lepton_tree->SetBranchAddress("n_OfLeptons", &num_of_leptons);
    lepton_tree->SetBranchAddress("n_Ofneutrinos", &num_of_neutrinos);
    lepton_tree->SetBranchAddress("lep_ids", &lep_ids);
    lepton_tree->SetBranchAddress("lep_px", &lep_px);
    lepton_tree->SetBranchAddress("lep_py", &lep_py);
    lepton_tree->SetBranchAddress("lep_pz", &lep_pz);
    lepton_tree->SetBranchAddress("lep_e", &lep_e);

    //lepton counter of each event
    vector<int> lepton_counter(6, 0);

    // Declaring wz candidate
    WZ_Pair WZ_candidate;

    //vectors for saving each lepton's variables
    vector<myParticle> leptons;

    // defining histograms
    TH1D *hZeeMassNoCuts = new TH1D("hZeeMassNoCuts", "Z_{ee} Mass No cuts", 30, 75, 115);
    TH1D *hZmmMassNoCuts = new TH1D("hZmmMassNoCuts", "Z_{mm} Mass No cuts", 30, 75, 115);
    TH1D *hZeeMass = new TH1D("hZeeMass", "Z_{ee} Mass After cuts", 30, 75, 115);
    TH1D *hZmmMass = new TH1D("hZmmMass", "Z_{mm} Mass After cuts", 30, 75, 115);
    TH1D *hmassZ = new TH1D("hmassZ", "Z Mass After Cuts", 30, 75, 115);
    TH1D *hPtZ = new TH1D("hPtZ", "Z p_{T} After Cuts", 40, 0, 400);

    TH1D *hmasstW = new TH1D("hmasstW", "W m_{T}^{W}", 20, 0, 200);
    TH1D *hPtW = new TH1D("hPtW", "W p_{T}", 50, 0, 400);
    TH1D *hWZtMass = new TH1D("hWZtMass", "WZ m_{T}^{WZ}", 34, 0, 700);
    TH1D *hWZeeMass = new TH1D("hWZeeMass", "WZ_{ee} Mass", 34, 0, 700);
    TH1D *hWZmmMass = new TH1D("hWZmmMass", "WZ_{mm} Mass", 34, 0, 700);

    TH1D *hWplusZTMass = new TH1D("hWplusZTMass", "W^{+}Z Mass", 34, 0, 700);
    TH1D *hWminusZTMass = new TH1D("hWminusZTMass", "W^{-}Z Mass", 34, 0, 700);
    TH1D *hWplusZeeTMass = new TH1D("hWplusZeeTMass", "W^{+}Z_{ee} Mass", 34, 0, 700);
    TH1D *hWminusZeeTMass = new TH1D("hWminusZeeTMass", "W^{-}Z_{ee} Mass", 34, 0, 700);
    TH1D *hWplusZmmTMass = new TH1D("hWplusZmmTMass", "W^{+}Z_{mm} Mass", 34, 0, 700);
    TH1D *hWminusZmmTMass = new TH1D("hWminusZmmTMass", "W^{-}Z_{mm} Mass", 34, 0, 700);

    TH1D *hWZeee = new TH1D("hWZeee", "WZ eee channel", 28, 0, 700);
    TH1D *hWZeem = new TH1D("hWZeem", "WZ eem channel", 28, 0, 700);
    TH1D *hWZmmm = new TH1D("hWZmmm", "WZ mmm channel", 28, 0, 700);
    TH1D *hWZmme = new TH1D("hWZmme", "WZ mme channel", 28, 0, 700);

    TH1D *hleptons_pt = new TH1D("hleptons_pt", "3 leptons pt before cuts", 20, 0, 220);
    TH1D *h_masslll = new TH1D("h_masslll", "Invariant mass of 3 leptons ", 20, 0, 800);
    TH1D *hwlepton_pt = new TH1D("hwlepton_pt", "W lepton pt before cuts", 50, 0, 300);
    TH1D *hwlepton_pt_after = new TH1D("hwlepton_pt_after", "W lepton pt after cuts", 50, 0, 300);

    TH1D *hfake_neutrino_pt = new TH1D("hfake_neutrino_pt", " Pt of fake neutrino", 20, 0, 300);
    TH1D *hreal_neutrino_pt = new TH1D("hreal_neutrino_pt", " Pt of real neutrino", 20, 0, 300);
    TH2D *neutrino_pts = new TH2D("neutrino_pts", "Real vs Fake neutrino Pt", 100, 0, 250, 100, 0, 250);
    TH2D *neutrino_phis = new TH2D("neutrino_phis", "Real vs Fake neutrino Phi", 100, -3.5, 3.5, 100, -3.5, 3.5);

    TH2D *zmassvs_wztmass = new TH2D("zmassvs_wztmass", "Z mass vs WZ tmass", 100, 70, 150, 100, 0, 1000);
    zmassvs_wztmass->GetXaxis()->SetTitle("Z Mass");
    zmassvs_wztmass->GetYaxis()->SetTitle("WZ Transverse Mass");

    TH2D *wtmassvs_wztmass = new TH2D("wtmassvs_wztmass", "W tmass vs WZ tmass", 100, 0, 700, 100, 0, 1000);
    wtmassvs_wztmass->GetXaxis()->SetTitle("W Transverse Mass");
    wtmassvs_wztmass->GetYaxis()->SetTitle("WZ Transverse Mass");

    TH1D *h_zleptons_deltaR = new TH1D("h_zleptons_deltaR", "#DeltaR12 between Z leptons", 10, 0, 5);
    TH1D *h_wzleptons_deltaR13 = new TH1D("h_zleptons_deltaR13", "#DeltaR13 between WZ leptons", 100, 0, 5);
    TH1D *h_wzleptons_deltaR23 = new TH1D("h_zleptons_deltaR23", "#DeltaR23 between WZ leptons", 100, 0, 5);
    TH1D *h_wz_deltay = new TH1D("h_wz_deltay", "#DeltaY between W and Z", 100, -6, 6);

    TObjArray Hlist(0);
    Hlist.Add(hZeeMass);
    Hlist.Add(hZmmMass);
    Hlist.Add(hmassZ);
    Hlist.Add(hPtZ);
    Hlist.Add(hmasstW);
    Hlist.Add(hPtW);
    Hlist.Add(hWZtMass);
    Hlist.Add(hWZeeMass);
    Hlist.Add(hWZmmMass);
    Hlist.Add(hWplusZTMass);
    Hlist.Add(hWminusZTMass);
    Hlist.Add(hWplusZeeTMass);
    Hlist.Add(hWminusZeeTMass);
    Hlist.Add(hWplusZmmTMass);
    Hlist.Add(hWminusZmmTMass);
    Hlist.Add(hWZeee);
    Hlist.Add(hWZeem);
    Hlist.Add(hWZmmm);
    Hlist.Add(hWZmme);
    // Hlist.Add(hleptons_pt);
    Hlist.Add(h_masslll);
    // Hlist.Add(neutrino_pts);
    // Hlist.Add(neutrino_phis);
    Hlist.Add(h_zleptons_deltaR);
    Hlist.Add(h_wz_deltay);
    Hlist.Add(hfake_neutrino_pt);
    Hlist.Add(hreal_neutrino_pt);

    int class_id;
    class_id = (str.find("wz") != string::npos) ? 1 : 0;

    // ofile << "Z boson Pt \t Z boson Mass \t W Pt \t W Transverse Mass \t Eta of W \t WZ Transverse Mass \t |y_wl - y_Z| \t Invariang mass of 3 leptons
    // \t Class S=1,B=0 \n";

    /*##################  main event loop #######################*/

    for (int iEntry = 0; iEntry < nEntries; iEntry++)
    {
        leptons.clear();
        WZ_candidate.ClearValues();
        int lcounter = 0;
        lepton_tree->GetEntry(iEntry);
        leptons.reserve(num_of_leptons);

        TLorentzVector sum_neutrino;
        double nu_px = 0.0, nu_py = 0.0, nu_pz = 0.0, nu_e = 0.0;
        for (int i = 0, range = lep_ids->size(); i < range; i++)
        {
            if (abs(lep_ids->at(i)) == 11 || abs(lep_ids->at(i)) == 13)
            {
                leptons.emplace_back(lep_ids->at(i), lep_px->at(i), lep_py->at(i), lep_pz->at(i), lep_e->at(i));
                lcounter++;
            }
            else
            {
                nu_px += lep_px->at(i);
                nu_py += lep_py->at(i);
                nu_pz += lep_pz->at(i);
                nu_e += lep_e->at(i);
            }
        }
        sum_neutrino.SetPxPyPzE(nu_px, nu_py, nu_pz, nu_e);

        
        if (num_of_leptons == 1)
        {
            lepton_counter[0]++;
        }
        else if (num_of_leptons == 2)
        {
            lepton_counter[1]++;
        }
        else if (num_of_leptons == 3)
        {
            lepton_counter[2]++;
        }
        else if (num_of_leptons == 4)
        {
            lepton_counter[3]++;
        }
        else if (num_of_leptons == 5)
        {
            lepton_counter[4]++;
        }
        else if (num_of_leptons == 6)
        {
            lepton_counter[5]++;
        }

        // for (const auto &lep : leptons)
        // {
        //     hleptons_pt->Fill(lep.Pt());
        // }
        lep_ids->clear();
        lep_px->clear();
        lep_py->clear();
        lep_pz->clear();
        lep_e->clear();
        lep_ids->shrink_to_fit();
        lep_px->shrink_to_fit();
        lep_px->shrink_to_fit();
        lep_px->shrink_to_fit();
        lep_e->shrink_to_fit();

        // calling the functions from WZ_checker
        WZ_candidate.SetValues(leptons, sum_neutrino);
        WZ_candidate.Leptonfinder();

        // clearing the containers passed at WZ_candidate
        leptons.clear();
        leptons.shrink_to_fit();
        

        /*######################################################## Filling histograms #################################################################################### */
        if (!WZ_candidate.skip_event)
        {

           

            if (WZ_candidate.isZ && WZ_candidate.ZMassCutsPass)
            {

                hwlepton_pt->Fill(WZ_candidate.wz_leptons[2].Pt());

                hmassZ->Fill(WZ_candidate.Z_Mass);
                hPtZ->Fill(WZ_candidate.Z_Pt);

                if (WZ_candidate.isZ && WZ_candidate.isW && WZ_candidate.isWZ && WZ_candidate.ZPtCutsPass && WZ_candidate.ZMassCutsPass && WZ_candidate.WlPtCutsPass && WZ_candidate.WtMassCutsPass && WZ_candidate.WZtMassCutsPass)
                {
                    hfake_neutrino_pt->Fill(WZ_candidate.fake_neutrino.Pt());
                    hreal_neutrino_pt->Fill(WZ_candidate.real_neutrino.Pt());

                    neutrino_pts->Fill(WZ_candidate.fake_neutrino.Pt(), WZ_candidate.real_neutrino.Pt());
                    neutrino_phis->Fill(WZ_candidate.fake_neutrino.Phi(), WZ_candidate.real_neutrino.Phi());

                    for (const auto &lep : WZ_candidate.wz_leptons)
                    {
                        hleptons_pt->Fill(lep.Pt());
                    }

                    ofile << WZ_candidate.Z_Pt << "\t" << WZ_candidate.Z_Mass << "\t" << WZ_candidate.W_Pt << "\t" << WZ_candidate.W_tMass << "\t"
                          << WZ_candidate.etaW << "\t" << WZ_candidate.WZ_tMass << "\t" << fabs(WZ_candidate.deltaPsi) << "\t" << WZ_candidate.masslll << "\t" << class_id << "\n";

                    h_wz_deltay->Fill(WZ_candidate.deltaPsi);
                    h_masslll->Fill(WZ_candidate.masslll);

                    // hwlepton_pt_after->Fill(WZ_candidate.wz_leptons[2].Pt());

                    h_zleptons_deltaR->Fill(WZ_candidate.deltaR.at(0).second);
                    h_wzleptons_deltaR13->Fill(WZ_candidate.deltaR.at(1).second);
                    h_wzleptons_deltaR23->Fill(WZ_candidate.deltaR.at(2).second);

                    hmasstW->Fill(WZ_candidate.W_tMass);
                    hWZtMass->Fill(WZ_candidate.WZ_tMass);
                    hPtW->Fill(WZ_candidate.W_Pt);
                    // Filling scatter plots
                    zmassvs_wztmass->Fill(WZ_candidate.Z_Mass, WZ_candidate.WZ_tMass);
                    wtmassvs_wztmass->Fill(WZ_candidate.W_tMass, WZ_candidate.WZ_tMass);

                    if (WZ_candidate.isWZeee)
                    {
                        hWZeee->Fill(WZ_candidate.WZ_tMass);
                    }
                    else if (WZ_candidate.isWZeem)
                    {
                        hWZeem->Fill(WZ_candidate.WZ_tMass);
                    }
                    else if (WZ_candidate.isWZmmm)
                    {
                        hWZmmm->Fill(WZ_candidate.WZ_tMass);
                    }
                    else if (WZ_candidate.isWZmme)
                    {
                        hWZmme->Fill(WZ_candidate.WZ_tMass);
                    }

                    if (WZ_candidate.isZ_ee)
                    {

                        hZeeMass->Fill(WZ_candidate.Z_Mass);

                        if (WZ_candidate.WZtMassCutsPass)
                        {
                            hWZeeMass->Fill(WZ_candidate.WZ_tMass);
                            if (WZ_candidate.isWZminus)
                            {
                                hWminusZeeTMass->Fill(WZ_candidate.WZ_tMass);
                            }

                            if (WZ_candidate.isWZplus)
                            {
                                hWplusZeeTMass->Fill(WZ_candidate.WZ_tMass);
                            }
                        }
                    }

                    if (WZ_candidate.isZ_mm)
                    {

                        hZmmMass->Fill(WZ_candidate.Z_Mass);

                        if (WZ_candidate.WZtMassCutsPass)
                        {
                            hWZmmMass->Fill(WZ_candidate.WZ_tMass);
                            if (WZ_candidate.isWZminus)
                            {
                                hWminusZmmTMass->Fill(WZ_candidate.WZ_tMass);
                            }

                            if (WZ_candidate.isWZplus)
                            {
                                hWplusZmmTMass->Fill(WZ_candidate.WZ_tMass);
                            }
                        }
                    }

                    if (WZ_candidate.isWZminus)
                    {
                        hWminusZTMass->Fill(WZ_candidate.WZ_tMass);
                    }

                    if (WZ_candidate.isWZplus)
                    {
                        hWplusZTMass->Fill(WZ_candidate.WZ_tMass);
                    }
                }
            }
        }
    }

    /* ################### Drawing histograms  ############################################### */

    c1->cd(1);
    hZeeMassNoCuts->GetXaxis()->SetTitle("Mass (GeV)");
    hZeeMassNoCuts->GetYaxis()->SetTitle("Events");
    hZeeMassNoCuts->DrawCopy("");

    c1->cd(2);
    hZmmMassNoCuts->GetXaxis()->SetTitle("Mass (GeV)");
    hZmmMassNoCuts->GetYaxis()->SetTitle("Events");
    hZmmMassNoCuts->DrawCopy("");

    c1->cd(3);
    hmassZ->GetXaxis()->SetTitle("Mass (GeV)");
    hmassZ->GetYaxis()->SetTitle("Events");
    hmassZ->DrawCopy("");

    c1->cd(4);
    hZeeMass->GetXaxis()->SetTitle("Mass (GeV)");
    hZeeMass->GetYaxis()->SetTitle("Events");
    hZeeMass->DrawCopy("");

    c1->cd(5);
    hZmmMass->GetXaxis()->SetTitle("Mass (GeV)");
    hZmmMass->GetYaxis()->SetTitle("Events");
    hZmmMass->DrawCopy("");

    c1->cd(6);
    hPtZ->GetXaxis()->SetTitle("p_{T}^{Z} (GeV)");
    hPtZ->GetYaxis()->SetTitle("Events");
    hPtZ->DrawCopy();

    /*########################################### WZ canvas ############################## */
    c2->cd(1);
    hmasstW->GetXaxis()->SetTitle("m_{T}^{W} (GeV)");
    hmasstW->GetYaxis()->SetTitle("Events");
    hmasstW->DrawCopy("");

    c2->cd(2);
    hPtW->GetXaxis()->SetTitle("p_{T}^{W} (GeV)");
    hPtW->GetYaxis()->SetTitle("Events");
    hPtW->DrawCopy();

    c2->cd(3);
    hWZtMass->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWZtMass->GetYaxis()->SetTitle("Events");
    hWZtMass->DrawCopy("");

    c2->cd(4);
    hWZeeMass->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWZeeMass->GetYaxis()->SetTitle("Events");
    hWZeeMass->DrawCopy("");

    c2->cd(5);
    hWZmmMass->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWZmmMass->GetYaxis()->SetTitle("Events");
    hWZmmMass->DrawCopy("");
    //################################################################################## //
    c3->cd(1);
    hWminusZeeTMass->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWminusZeeTMass->GetYaxis()->SetTitle("Events");
    hWminusZeeTMass->DrawCopy("");

    c3->cd(2);
    hWminusZmmTMass->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWminusZmmTMass->GetYaxis()->SetTitle("Events");
    hWminusZmmTMass->DrawCopy("");

    c3->cd(3);
    hWminusZTMass->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWminusZTMass->GetYaxis()->SetTitle("Events");
    hWminusZTMass->DrawCopy("");

    c3->cd(4);
    hWplusZeeTMass->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWplusZeeTMass->GetYaxis()->SetTitle("Events");
    hWplusZeeTMass->DrawCopy("");

    c3->cd(5);
    hWplusZmmTMass->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWplusZmmTMass->GetYaxis()->SetTitle("Events");
    hWplusZmmTMass->DrawCopy("");

    c3->cd(6);
    hWplusZTMass->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWplusZTMass->GetYaxis()->SetTitle("Events");
    hWplusZTMass->DrawCopy("");

    /*####################################################################################*/
    // c4->cd(1);
    // hwznocutsm->DrawCopy();
    // c4->cd(2);
    // hwznocutsp->DrawCopy();
    /*####################################################################################*/
    c5->cd(1);
    hWZeee->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWZeee->GetYaxis()->SetTitle("Events");
    hWZeee->DrawCopy();
    c5->cd(2);
    hWZeem->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWZeem->GetYaxis()->SetTitle("Events");
    hWZeem->DrawCopy();
    c5->cd(3);
    hWZmmm->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWZmmm->GetYaxis()->SetTitle("Events");
    hWZmmm->DrawCopy();
    c5->cd(4);
    hWZmme->GetXaxis()->SetTitle("m_{T}^{WZ} (GeV)");
    hWZmme->GetYaxis()->SetTitle("Events");
    hWZmme->DrawCopy();
    /*####################################################################################*/
    c6->cd(1);
    hleptons_pt->GetXaxis()->SetTitle("p_{T}");
    hleptons_pt->GetXaxis()->SetTitle("Leptons");
    hleptons_pt->DrawCopy();
    c6->cd(2);
    h_masslll->GetXaxis()->SetTitle("p_{T}");
    h_masslll->GetXaxis()->SetTitle("Leptons");
    h_masslll->DrawCopy();
    c6->cd(3);
    hwlepton_pt->GetXaxis()->SetTitle("p_{T}");
    hwlepton_pt->GetXaxis()->SetTitle("Leptons");
    hwlepton_pt->DrawCopy();
    c6->cd(4);
    hwlepton_pt_after->GetXaxis()->SetTitle("p_{T}");
    hwlepton_pt_after->GetXaxis()->SetTitle("Leptons");
    hwlepton_pt_after->DrawCopy();
    /*#####################################################################################*/
    c7->cd(1);
    hfake_neutrino_pt->DrawCopy();
    c7->cd(2);
    hreal_neutrino_pt->DrawCopy("");
    c7->cd(3);
    neutrino_pts->DrawCopy();
    c7->cd(4);
    neutrino_phis->DrawCopy();
    /*#####################################################################################*/
    TCanvas *neutrino_canvas = new TCanvas("neutrino_canvas", "", 2400, 1440);
    neutrino_canvas->Divide(2);
    TH1D *proj_ptx = neutrino_pts->ProjectionX();
    TH1D *proj_pty = neutrino_pts->ProjectionY();
    neutrino_canvas->cd(1);
    proj_ptx->DrawCopy();
    neutrino_canvas->cd(2);
    proj_pty->DrawCopy();
    /*#####################################################################################*/

    // TCanvas *c_leptons_pt = new TCanvas("c_leptons_pt", "", 1200, 720);
    // c_leptons_pt->Divide(2);
    // c_leptons_pt->cd(1);
    // h_2wleptons_pt->DrawCopy();
    // h_2wleptons_pt->GetYaxis()->SetTitle("Events");
    // c_leptons_pt->cd(2);
    // h_bleptons_pt->DrawCopy();
    // h_bleptons_pt->GetYaxis()->SetTitle("Events");
    /*#####################################################################################*/

    TCanvas *c_deltaR = new TCanvas("c_deltaR", "#DeltaR of leptons", 2400, 1440);
    c_deltaR->Divide(2, 2);
    c_deltaR->cd(1);
    h_zleptons_deltaR->DrawCopy();
    c_deltaR->cd(2);
    h_wzleptons_deltaR13->DrawCopy();
    c_deltaR->cd(3);
    h_wzleptons_deltaR23->DrawCopy();
    c_deltaR->cd(4);
    h_wz_deltay->DrawCopy();
    /*#####################################################################################*/

    // writing cross section to output file
    info_tree->GetEntry(0);
    string output_filename = str + "_output.root";
    TFile output_file(output_filename.c_str(), "RECREATE");
    TTree *sigma = new TTree("sigma", "Cross section in mb");
    sigma->Branch("cross_section", &cross_section);
    sigma->Branch("cross_section_error", &cross_section_error);
    sigma->Fill();
    Hlist.Write();
    output_file.Write();
    output_file.Close();

    // closing root file
    i_file.Close();

    delete c1;
    delete c2;
    delete c3;
    delete c5;
    delete c6;
    delete c7;
    delete neutrino_canvas;
    delete c_deltaR;
    delete lep_ids;
    delete lep_px;
    delete lep_py;
    delete lep_pz;
    delete lep_e;

    const auto done = chrono::steady_clock::now();

    cout << "Analysis for file : " << str << endl;
    cout << "################################################################################" << endl;
    cout << "Number of events with 3 lepton with Pt > 5.0        : " << setw(8) << WZ_candidate.n_of3lepton_events << setw(7) << " of " << setw(7) << nEntries << "\n";
    cout << "Number of events with leading lepton with Pt > 27.0 : " << setw(8) << WZ_candidate.n_ofleading_lepton << setw(7) << " of " << setw(7) << WZ_candidate.n_of3lepton_events << "\n";
    cout << "Number of events passing Zlepton Pt cuts            : " << setw(8) << WZ_candidate.n_ofZPtPass << setw(7) << " of " << setw(7) << WZ_candidate.n_ofleading_lepton << "\n";
    cout << "Number of events with Z lepton pair                 : " << setw(8) << WZ_candidate.n_ofZ_lepton_pairs << setw(7) << " of " << setw(7) << WZ_candidate.n_ofZPtPass << "\n";
    cout << "Number of events passing Zmass cut                  : " << setw(8) << WZ_candidate.n_ofZMassPass << setw(7) << " of " << setw(7) << WZ_candidate.n_ofZ_lepton_pairs << "\n";
    cout << "Number of events passing Wlepton Pt cuts            : " << setw(8) << WZ_candidate.n_ofWPtPass << setw(7) << " of " << setw(7) << WZ_candidate.n_ofZMassPass << "\n";
    cout << "Number of events passing W Transverse Mass cuts     : " << setw(8) << WZ_candidate.n_ofWtMassPass << setw(7) << " of " << setw(7) << WZ_candidate.n_ofWPtPass << "\n";
    cout << "Number of events with isolated WZ leptons           : " << setw(8) << WZ_candidate.n_ofIsolatedLeptons << setw(7) << " of " << setw(7) << WZ_candidate.n_ofWtMassPass << "\n";
    cout << "Analysis running time                               : " << setw(8) << chrono::duration<double>(done - start).count() << " seconds" << endl;
    cout << "################################################################################" << endl;
    ofile.close();

    for (int i = 0, range = lepton_counter.size(); i < range; i++)
    {
        cout << "Number of event with " << i + 1 << " leptons :" << lepton_counter[i] << "\n";
    }
}

int main(int argc, char *argv[])
{
    for (int i = 1; i < argc; i++)
    {
        wz_event_selector(argv[i]);
    }
    return 0;
}