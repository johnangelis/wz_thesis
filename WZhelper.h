#pragma once

#include <iostream>
#include <stdexcept>
#include <cmath>
#include <string>
#include <algorithm>
#include <vector>
#include <complex>
#include "myParticle.h"

using namespace TMath;

constexpr double leadleptonPt = 27;   // Leading lepton minimum Pt
constexpr double ZmPdg = 91.1876;     // PDG mass for Z in Gev/c^2
constexpr double WmPdg = 80.385;      // PDG mass for W in Gev/c^2
constexpr double leptonPtCut = 15.0;  // Cut on transverse momentum for Z leptons
constexpr double leptonEtaCut = 2.5;  // Cut on eta for all leptons
constexpr double WleptonPtCut = 20.0; // Cut on transverse momentum for W lepton
constexpr double WtMassCut = 30.0;    // Cut on transverse mass of W

constexpr double GammaZ = 2.4952;
constexpr double GammaW = 2.085;

class WZ_Pair
{
  public:
    double Z_Mass;   // Z mass after Pt,Eta and Zmpdg cuts
    double Z_Pt;     // Z transverse momentum
    double W_Pt;     // W trasverse momentum
    double W_tMass;  // W transverse mass
    double WZ_tMass; // WZ transverse mass
    double wlepton_pt = 0.0;
    double wlepton_pt_after = 0.0;

    // variables for nn
    double deltaPsi = 0.0;
    double etaW = 0.0;
    double masslll = 0.0;

    // Initializing Counters
    static int n_of3lepton_events;  // Counter of events with 3 leptons with Pt > 7 GeV
    static int n_ofleading_lepton;  // Coutner of events with leading leptons with Pt > 27 GeV
    static int n_ofZ_lepton_pairs;  // Counter of events with Z lepton pair
    static int n_ofZPtPass;         // Counter of pairs passing Z lepton Pt cuts
    static int n_ofZMassPass;       // Counter of pairs passing Z mass cuts
    static int n_ofWPtPass;         // Counter of events passing W Pt cuts
    static int n_ofWtMassPass;      // Counter of events passing W Transverse Mass cuts
    static int n_ofIsolatedLeptons; // Counter of events with isolated WZ leptons

    bool skip_event = false;
    bool isZ = false;                // Flag for checking if there is found a Z pair
    bool isW = false;                // Flag for checking if there is found a W lepton
    bool isWZ = false;               // Flag for checking if there is founda WZ pair
    bool isZ_ee = false;             // Flag for Z-> ee events
    bool isZ_mm = false;             // Flag for Z-> mm events
    bool isWZplus = false;           // Flag for positive WZ
    bool isWZminus = false;          // Flag for negative WZ
    bool ZPtCutsPass = false;        // Flag for passing Z cuts on Pt and Eta
    bool ZMassCutsPass = false;      // Flag for passing Zpdg mass
    bool WlPtCutsPass = false;       // Flag for passing W Pt cut
    bool WtMassCutsPass = false;     // Flag for passing W Mt cut
    bool WZtMassCutsPass = false;    // Flag for passing WZ transverse mass cut
    bool isolated_wzleptons = false; // Flag for DR of the 3 WZ leptons

    // Flags for the 4 lepton channels of WZ
    bool isWZeee = false;
    bool isWZeem = false;
    bool isWZmmm = false;
    bool isWZmme = false;

    WZ_Pair(){};
    void SetValues(const std::vector<myParticle> &, const TLorentzVector &);
    void CheckWZ(void);
    void Leptonfinder(void);
    void TypeOfZ(void);
    void TypeOfW(void);
    void TypeOfWZ(void);
    bool CheckZPtCuts(const std::vector<myParticle> &);
    bool CheckZmass(void);
    void CheckWLPt(void);
    void CheckWtmass(const myParticle &, const TLorentzVector &);
    void WZTransverseMass(void);
    void ClearValues(void);
    void ZLepton_Estimator(const std::vector<myParticle> &);
    void CompareZ(const std::vector<std::pair<myParticle, myParticle>> &);
    bool WZleptons_isolation(void);
    bool isBaseline(const myParticle &) const;
    bool isZLepton(const myParticle &) const;

    ~WZ_Pair()
    {
        n_of3lepton_events = 0;
        n_ofZ_lepton_pairs = 0;
        n_ofleading_lepton = 0;
        n_ofZPtPass = 0;
        n_ofZMassPass = 0;
        n_ofWPtPass = 0;
        n_ofWtMassPass = 0;
        n_ofIsolatedLeptons = 0;
        ClearValues();
    };

    std::vector<myParticle> wz_leptons; // vector for storing the TLorentzVector for the 3 WZ leptons
    TLorentzVector fake_neutrino;       // vector for the fake_neutrino variables
    TLorentzVector real_neutrino;
    std::vector<std::pair<std::string, double>> deltaR;

  private:
    std::vector<myParticle> wz_lepton_candidates; // vector for storing the TLorentzVectors for all the leptons of every event
    int wlepton_candidates_id = 0;
    myParticle Z, W;
};
// initializing the counters
int WZ_Pair::n_of3lepton_events = 0;
int WZ_Pair::n_ofleading_lepton = 0;
int WZ_Pair::n_ofZPtPass = 0;
int WZ_Pair::n_ofZ_lepton_pairs = 0;
int WZ_Pair::n_ofZMassPass = 0;
int WZ_Pair::n_ofWPtPass = 0;
int WZ_Pair::n_ofWtMassPass = 0;
int WZ_Pair::n_ofIsolatedLeptons = 0;

/* ################################################################
Declaring class member functions
######################################################################*/
// Function for setting initial values of the leptons' variables
void WZ_Pair::SetValues(const std::vector<myParticle> &Leptons, const TLorentzVector &sum_neutrino)
{
    std::copy(Leptons.cbegin(), Leptons.cend(), std::back_inserter(wz_lepton_candidates));
    real_neutrino = sum_neutrino;
    wz_leptons.reserve(3);
    deltaR.reserve(3);
}

bool WZ_Pair::isBaseline(const myParticle &lep) const
{
    if (lep.Pt() > 5.0)
    {
        if (abs(lep.getId()) == 11 && fabs(lep.Eta()) < leptonEtaCut)
        {
            return true;
        }
        else if (abs(lep.getId()) == 13 && fabs(lep.Eta()) < 2.7)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

bool WZ_Pair::isZLepton(const myParticle &lep) const
{
    if (lep.Pt() > leptonPtCut)
    {
        if (abs(lep.getId()) == 13 && fabs(lep.Eta()) < leptonEtaCut)
        {
            return true;
        }
        else if (abs(lep.getId()) == 11)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}


// implementing the z candidates comparison
void WZ_Pair::Leptonfinder()
{
    std::vector<myParticle> accepted_leptons;
    accepted_leptons.reserve(wz_lepton_candidates.size());
    std::for_each(wz_lepton_candidates.begin(), wz_lepton_candidates.end(), [&](const myParticle &x) { if(isBaseline(x)) accepted_leptons.emplace_back(x); });

    if (accepted_leptons.size() != 3)
    {
        //skip this event
        isZ = false;
        isW = false;
        isWZ = false;
        skip_event = true;
    }
    else if (accepted_leptons.size() == 3)
    {
        n_of3lepton_events++;
        bool is_leading_lepton;
        for (const auto &veclep : accepted_leptons)
        {
            if (veclep.Pt() > leadleptonPt)
            {
                is_leading_lepton = true;
                n_ofleading_lepton++;
                break;
            }
            else
            {
                is_leading_lepton = false;
                isZ = false;
                isW = false;
                isWZ = false;
                // skip_event = true;
            }
        }

        if (is_leading_lepton && CheckZPtCuts(accepted_leptons))
        {
            std::vector<std::pair<myParticle, myParticle>> Z_candidates_pair;
            Z_candidates_pair.reserve(3);
            // for loop for finding candidate Z lepton pairs
            for (int i = 0; i < 3; i++)
            {
                for (int j = i + 1; j < 3; j++)
                {
                    if ((accepted_leptons.at(i).getId() + accepted_leptons.at(j).getId()) == 0)
                    {
                        Z_candidates_pair.emplace_back(accepted_leptons.at(i), accepted_leptons.at(j));
                    }
                }
            }

            //checking if there is any Z candidate
            if (Z_candidates_pair.size() > 0)
            {
                CompareZ(Z_candidates_pair); //function for finding the fittest Z lepton pair

                // ZLepton_Estimator(accepted_leptons); // function for finding Z lepton pair with the Estimator
            }
            else
            {
                isZ = false;
                isW = false;
                isWZ = false;
            }

            if (isZ && CheckZmass())
            {
                myParticle wlepton_candidate;

                for (const auto &acc_lep : accepted_leptons)
                {
                    if (acc_lep != wz_leptons.at(0) && acc_lep != wz_leptons.at(1))
                    {
                        wlepton_candidate = acc_lep;
                    }
                }
                if (abs(wlepton_candidate.getId()) == 11 || abs(wlepton_candidate.getId()) == 13)
                {
                    wlepton_pt_after = wlepton_candidate.Pt();

                    wz_leptons.emplace_back(wlepton_candidate);
                    if (wz_leptons.size() != 3)
                    {
                        std::cout << "error" << std::endl;
                    }
                    CheckWLPt();

                    TypeOfZ();
                    TypeOfW();
                    CheckWZ();
                }
                else
                {
                    isWZ = false;
                    isW = false;
                }
            }
            else
            {

                isWZ = false;
                isW = false;
            }
        }
        else
        {
            isZ = false;
            isW = false;
            isWZ = false;
        }
    }
}

// Z lepton estimator
void WZ_Pair::ZLepton_Estimator(const std::vector<myParticle> &leptons)
{
    std::vector<std::pair<myParticle, myParticle>> Z_candidates_pair;
    Z_candidates_pair.reserve(3);
    for (int i = 0; i < 3; i++)
    {
        for (int j = i + 1; j < 3; j++)
        {
            if ((leptons.at(i).getId() + leptons.at(j).getId()) == 0)
            {
                Z_candidates_pair.emplace_back(leptons.at(i), leptons.at(j));
            }
        }
    }

    std::vector<myParticle> w_lepton;
    std::vector<TLorentzVector> neutrino_vector;
    for (int j = 0, range1 = Z_candidates_pair.size(); j < range1; j++)
    {
        for (int i = 0, range2 = leptons.size(); i < range2; i++)
        {
            if (leptons.at(i) != Z_candidates_pair.at(j).first && leptons.at(i) != Z_candidates_pair.at(j).second)
            {
                w_lepton.emplace_back(leptons.at(i));
            }
        }
    }

    for (int i = 0, range = Z_candidates_pair.size(); i < range; i++)
    {
        double pt, px, py;
        pt = Z_candidates_pair.at(i).first.Pt() + Z_candidates_pair.at(i).second.Pt() + w_lepton.at(i).Pt();
        px = Z_candidates_pair.at(i).first.Px() + Z_candidates_pair.at(i).second.Px() + w_lepton.at(i).Px();
        py = Z_candidates_pair.at(i).first.Py() + Z_candidates_pair.at(i).second.Py() + w_lepton.at(i).Py();

        neutrino_vector.emplace_back(-px, -py, 0, pt);
    }

    std::vector<double> P_est;
    P_est.reserve(3);

    for (int i = 0, range = Z_candidates_pair.size(); i < range; i++)
    {
        std::complex<double> d1(std::pow((Z_candidates_pair.at(i).first + Z_candidates_pair.at(i).second).M(), 2) - std::pow(ZmPdg, 2), GammaZ * ZmPdg);
        std::complex<double> d2(std::pow((w_lepton.at(i) + neutrino_vector.at(i)).M(), 2) - std::pow(WmPdg, 2), GammaW * WmPdg);
        P_est.push_back(std::pow(std::abs(1.0 / d1), 2) * std::pow(std::abs(1.0 / d2), 2));
    }

    const auto maxP = std::max_element(P_est.cbegin(), P_est.cend());
    int max_est = std::distance(P_est.cbegin(), maxP);
    n_ofZ_lepton_pairs++;
    wz_leptons.emplace_back(Z_candidates_pair.at(max_est).first);
    wz_leptons.emplace_back(Z_candidates_pair.at(max_est).second);
    isZ = true;
}

// Function for finding the optimal Z leptons
void WZ_Pair::CompareZ(const std::vector<std::pair<myParticle, myParticle>> &Z_candid)
{
    auto find_min_diff = [](std::pair<myParticle, myParticle> const &lhs, std::pair<myParticle, myParticle> const &rhs) {
        return (fabs((lhs.first + lhs.second).M() - ZmPdg) < fabs((rhs.first + rhs.second).M() - ZmPdg));
    };
    const auto minZ = std::min_element(Z_candid.cbegin(), Z_candid.cend(), find_min_diff); // iterator for finding the pair with minimum mass
    int min_pos = std::distance(Z_candid.cbegin(), minZ);
    n_ofZ_lepton_pairs++;
    wz_leptons.emplace_back(Z_candid.at(min_pos).first);
    wz_leptons.emplace_back(Z_candid.at(min_pos).second);
    isZ = true;
}



// function for checking cuts on Pt and eta of Z leptons
bool WZ_Pair::CheckZPtCuts(const std::vector<myParticle> &leptons_candidates)
{
    int leptons_passing_zpt_cuts = 0;
    for (const auto &lep_cand : leptons_candidates)
    {
        if (isZLepton(lep_cand))
        {
            leptons_passing_zpt_cuts++;
        }
    }

    if (leptons_passing_zpt_cuts == 3 /*&& leptons_candidates.size() == 3*/)
    {
        ZPtCutsPass = true; // passing cuts
        n_ofZPtPass++;
    }
    else
    {
        ZPtCutsPass = false; //not passing cuts
        isZ = false;
    }

    return ZPtCutsPass;
}

// // function for checking Zmass boundaries
bool WZ_Pair::CheckZmass()
{
    // myParticle Z;
    Z = wz_leptons.at(0) + wz_leptons.at(1);

    if (Z.M() < 0.0)
    {
        std::cout << "Error! Negative Z Mass!! \n";
    }

    Z_Mass = Z.M();
    Z_Pt = Z.Pt();
    if (fabs(Z.M() - ZmPdg) < 10.0)
    {

        ZMassCutsPass = true;
        n_ofZMassPass++;
    }
    else
    {
        ZMassCutsPass = false;
        isWZ = false;
    }
    return ZMassCutsPass;
}

// function fot checking W leptons' Pt cuts
void WZ_Pair::CheckWLPt()
{
    double pt, px, py;
    pt = wz_leptons.at(0).Pt() + wz_leptons.at(1).Pt() + wz_leptons.at(2).Pt();
    px = wz_leptons.at(0).Px() + wz_leptons.at(1).Px() + wz_leptons.at(2).Px();
    py = wz_leptons.at(0).Py() + wz_leptons.at(1).Py() + wz_leptons.at(2).Py();

    fake_neutrino.SetPxPyPzE(-px, -py, 0, pt);
    // fake_neutrino = real_neutrino;

    if ((wz_leptons.at(2).Pt() > WleptonPtCut) /*&& (fabs(wz_leptons.at(2).Eta()) < leptonEtaCut)*/)
    {
        isW = true;
        WlPtCutsPass = true;
        n_ofWPtPass++;
        CheckWtmass(wz_leptons.at(2), fake_neutrino);
        // CheckWtmass(wz_leptons.at(2), real_neutrino);
    }
    else
    {
        isW = false;
        isWZ = false;
        // no pass at W Pt cuts
        WlPtCutsPass = false;
    }
}

// function for checking W Transverse Mass cuts
void WZ_Pair::CheckWtmass(const myParticle &WL1, const TLorentzVector &WL2)
{

    W_tMass = sqrt(2.0 * WL1.Pt() * WL2.Pt() * (1.0 - cos(WL1.DeltaPhi(WL2))));
    // W_tMass = (WL1 + WL2).Mt();
    // std::cout << (WL1 + WL2).Mt() << "\n";
    if (W_tMass > WtMassCut)
    {
        W = WL1 + WL2;
        WtMassCutsPass = true;
        isW = true;
        n_ofWtMassPass++;

        deltaPsi = (wz_leptons.at(2).Rapidity() - Z.Rapidity());
        etaW = W.Eta();
        masslll = (wz_leptons.at(0) + wz_leptons.at(1) + wz_leptons.at(2)).M();
    }
    else
    {
        isW = false;
        WtMassCutsPass = false;
        isWZ = false;
    }
}

// function for calculation of the WZ transverse mass
void WZ_Pair::WZTransverseMass()
{
    myParticle W;
    W = wz_leptons.at(2) + fake_neutrino;
    // W = wz_leptons.at(2) + real_neutrino;
    W_Pt = W.Pt();

    double pT, pX, pY;

    pT = wz_leptons.at(0).Pt() + wz_leptons.at(1).Pt() + wz_leptons.at(2).Pt();
    pX = wz_leptons.at(0).Px() + wz_leptons.at(1).Px() + wz_leptons.at(2).Px();
    pY = wz_leptons.at(0).Py() + wz_leptons.at(1).Py() + wz_leptons.at(2).Py();

    if (ZPtCutsPass && ZMassCutsPass && isZ && WtMassCutsPass && isW && WlPtCutsPass)
    {
        WZ_tMass = std::sqrt(pow(pT + fake_neutrino.Pt(), 2) - pow(pX + fake_neutrino.Px(), 2) - pow(pY + fake_neutrino.Py(), 2));
        // WZ_tMass = std::sqrt(pow(pT + real_neutrino.Pt(), 2) - pow(pX + real_neutrino.Px(), 2) - pow(pY + real_neutrino.Py(), 2));

        double delta_r12 = wz_leptons.at(0).DeltaR(wz_leptons.at(1));
        double delta_r13 = wz_leptons.at(0).DeltaR(wz_leptons.at(2));
        double delta_r23 = wz_leptons.at(1).DeltaR(wz_leptons.at(2));

        deltaR.emplace_back("DR12", delta_r12);
        deltaR.emplace_back("DR13", delta_r13);
        deltaR.emplace_back("DR23", delta_r23);

        // if (WZleptons_isolation())
        {
            n_ofIsolatedLeptons++;
            WZtMassCutsPass = true;
            isWZ = true;
        }
        // else
        // {
        //     isWZ = false;
        // }

        TypeOfWZ();
    }
    else
    {
        WZtMassCutsPass = false;
        isWZ = false;
    }
}

// function for calling all the other functions of checking process
void WZ_Pair::CheckWZ()
{

    if (ZPtCutsPass && ZMassCutsPass && isZ && WlPtCutsPass && WtMassCutsPass && isW)
    {
        // CheckWLPt();
        WZTransverseMass();
    }
    else
    {
        // ZPtCutsPass = false;
        // ZMassCutsPass = false;
        WlPtCutsPass = false;
        WtMassCutsPass = false;
        WZtMassCutsPass = false;
        isWZ = false;
        isZ = false;
        isW = false;
    }
}

// Function for defining the type of Z
void WZ_Pair::TypeOfZ()
{
    if (abs(wz_leptons.at(0).getId()) == 11)
    {
        isZ_ee = true;
        isZ_mm = false;
    }
    else if (abs(wz_leptons.at(0).getId()) == 13)
    {
        isZ_ee = false;
        isZ_mm = true;
    }
}

// Function for defining the type of W
void WZ_Pair::TypeOfW()
{
    if (wz_leptons.at(2).getId() > 0)
    {
        isWZminus = true;
        isWZplus = false;
    }
    else if (wz_leptons.at(2).getId() < 0)
    {
        isWZminus = false;
        isWZplus = true;
    }
}

void WZ_Pair::TypeOfWZ()
{
    if (isZ_ee && (abs(wz_leptons.at(2).getId()) == 11))
    {
        isWZeem = false;
        isWZmmm = false;
        isWZmme = false;
        isWZeee = true;
    }
    else if (isZ_ee && (abs(wz_leptons.at(2).getId()) == 13))
    {
        isWZeee = false;
        isWZmmm = false;
        isWZmme = false;
        isWZeem = true;
    }
    else if (isZ_mm && (abs(wz_leptons.at(2).getId()) == 13))
    {
        isWZeee = false;
        isWZeem = false;
        isWZmme = false;
        isWZmmm = true;
    }
    else if (isZ_mm && (abs(wz_leptons.at(2).getId()) == 11))
    {
        isWZeee = false;
        isWZeem = false;
        isWZmmm = false;
        isWZmme = true;
    }
}

bool WZ_Pair::WZleptons_isolation()
{
    isolated_wzleptons = true;
    if (deltaR.at(0).second > 0.2 && deltaR.at(1).second > 0.3 && deltaR.at(2).second > 0.3)
    {
        if (deltaR.at(2).second < 0.3)
        {
            std::cout << "error" << std::endl;
        }
        isolated_wzleptons = true;
    }
    else
    {
        isolated_wzleptons = false;
    }
    return isolated_wzleptons;
}

// Function for clearing the initial values
void WZ_Pair::ClearValues()
{
    // clearing all values for the next event
    Z_Mass = 0.0;
    Z_Pt = 0.0;
    W_Pt = 0.0;
    W_tMass = 0.0;
    WZ_tMass = 0.0;
    wlepton_pt = 0.0;
    wlepton_pt_after = 0.0;

    skip_event = false;

    isZ = false;
    isW = false;
    isWZ = false;
    isZ_ee = false; //for Z-> ee
    isZ_mm = false; //for Z-> mm
    isWZplus = false;
    isWZminus = false;
    isWZeee = false;
    isWZeem = false;
    isWZmme = false;
    isWZmmm = false;
    ZPtCutsPass = false;
    ZMassCutsPass = false;
    WlPtCutsPass = false;
    WtMassCutsPass = false;
    WZtMassCutsPass = false;

    wz_leptons.clear();
    wz_leptons.shrink_to_fit();
    fake_neutrino.Clear();
    real_neutrino.Clear();

    wz_lepton_candidates.clear();
    wlepton_candidates_id = 0;
    wz_lepton_candidates.shrink_to_fit();

    deltaR.clear();
    deltaR.shrink_to_fit();

    W.clear();
    Z.clear();
}

// #endif